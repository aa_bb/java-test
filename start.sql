CREATE DATABASE IF NOT EXISTS java_test;

CREATE TABLE IF NOT EXISTS public.student
(
    id integer NOT NULL DEFAULT nextval('student_id_seq'::regclass),
    name text COLLATE pg_catalog."default",
    passport text COLLATE pg_catalog."default",
    CONSTRAINT student_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)

INSERT INTO students (name, passport) VALUES ('Иванов И.И', '1234 567890'), ('Петров Ж.Ж.', '0987 654321'),
 ('Чижиков А.И.', '5555 555555');

