package ru.java_test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;

@Controller
public class StudentsController {

	@Autowired
	StudentRepository repository;

	@RequestMapping("/student/{id}")
	public String studentGet(@PathVariable Long id, Model model) {
        model.addAttribute("student", repository.findById(id).orElse(null));
        return "student";
	}

    @RequestMapping("/student/{id}/update")
    public String studentUpdate(@PathVariable Long id, @RequestParam String name, @RequestParam String passport) {
	    for (Student s : repository.findAll()) {
	        if (!s.getName().equals(name) && s.getPassport().equals(passport)) {
                return "redirect:/students";
            }
        }
        Student student = repository.findById(id).orElse(null);
        if (student != null) {
            student.setName(name);
            student.setPassport(passport);
        }
        repository.save(student);
        return "redirect:/students";
    }

    @RequestMapping("/student/{id}/delete")
    public String studentDelete(@PathVariable Long id) {
        repository.findById(id).ifPresent(student -> repository.delete(student));
        return "redirect:/students";
    }

    @RequestMapping(value="/students",method=RequestMethod.GET)
	public String studentsList(Model model) {
        model.addAttribute("students", repository.findAll());
        return "students";
	}

    @RequestMapping(value="/students",method=RequestMethod.POST)
	public String studentsAdd(@RequestParam String name, @RequestParam String passport, Model model) {
        for (Student s : repository.findAll()) {
            if (!s.getName().equals(name) && s.getPassport().equals(passport)) {
                return studentsList(model);
            }
        }
        Student newStudent = new Student();
        newStudent.setName(name);
        newStudent.setPassport(passport);
        repository.save(newStudent);

        return studentsList(model);
	}
}
